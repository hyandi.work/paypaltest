<?php
/**
 * Created by IntelliJ IDEA.
 * User: codeonward2
 * Date: 08.11.16
 * Time: 17:23
 */

namespace frontend\controllers;


use common\models\Payment;
use yii\web\Controller;

class PaypalController extends Controller
{

    public function actionSuccess()
    {
        //success=true&paymentId=PAY-0K816640HC271191DLARNCLI&token=EC-5T549690TE683680P&PayerID=63SM9MQUWNB5N
        $payment_id = \Yii::$app->request->get('paymentId');
        $token = \Yii::$app->request->get('paymentId');
        $payer_id = \Yii::$app->request->get('PayerID');
        if((\Yii::$app->request->get('success') == 'true') && $payment_id && $token && $payer_id){
            $payment = Payment::findByRequestID($payment_id);
            if($payment){
                $payment->execute($payment_id, $token, $payer_id);
                echo 'Success';
            }
        }
        if (Payment::isPay()){
            $this->redirect('/paypal/upload');
        }
    }

    public function actionFail()
    {
        echo 'Error';
    }

    public function actionPay()
    {
        if (Payment::isPay()){
            $this->redirect('/paypal/upload');
        }else {
            $payment = new Payment();
            $this->redirect($payment->pay());
        }
    }

    public function actionUpload()
    {
        if (Payment::isPay()) {
            \Yii::$app->response->content = 'Download <a href="/paypal/upload?file=true">file</a>';
            if(\Yii::$app->request->get('file')) {
                \Yii::$app->response->sendFile($path = \Yii::getAlias('@webroot') . '/robots.txt.zip');
            }
        }
    }
}