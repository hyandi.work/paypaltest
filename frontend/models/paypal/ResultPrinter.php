<?php
namespace frontend\models\paypal;
/*
    Common functions used across samples
*/

/**
 * Helper Class for Printing Results
 *
 * Class ResultPrinter
 */
class ResultPrinter
{

    private static $printResultCounter = 0;

    /**
     * Prints HTML Output to web page.
     *
     * @param string     $title
     * @param string    $objectName
     * @param string    $objectId
     * @param mixed     $request
     * @param mixed     $response
     * @param string $errorMessage
     */
    public static function printOutput($title, $objectName, $objectId = null, $request = null, $response = null, $errorMessage = null)
    {
        if (PHP_SAPI == 'cli') {
            self::$printResultCounter++;
            printf("\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
            printf("(%d) %s", self::$printResultCounter, strtoupper($title));
            printf("\n-------------------------------------------------------------\n\n");
            if ($objectId) {
                printf("Object with ID: %s \n", $objectId);
            }
            printf("-------------------------------------------------------------\n");
            printf("\tREQUEST:\n");
            self::printConsoleObject($request);
            printf("\n\n\tRESPONSE:\n");
            self::printConsoleObject($response, $errorMessage);
            printf("\n-------------------------------------------------------------\n\n");
        } else {
            if (self::$printResultCounter == 0) {
                echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <meta name=\"description\" content=\"\">
    <meta name=\"author\" content=\"\">
    <link rel=\"icon\" href=\"images/favicon.ico\">

    <title>PayPal REST API Samples</title>

    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css\">
    <link href=\"//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css\" rel=\"stylesheet\">
    <style>
        body {
            font-family: \"Helvetica Neue\", Helvetica, Arial, sans-serif;
            -webkit-font-smoothing: antialiased;
        }
        pre {
            overflow-y: auto;
            overflow-wrap: normal;
        }
        pre.error, div.error, p.error {
            border-color: red;
            color: red;
            overflow-y: visible;
            overflow-wrap: break-word;
        }
        .panel-default>.panel-heading.error {
            background-color: red;
            color: white;
        }
        .panel-title a, .home{
            font-family: Menlo,Monaco,Consolas,\"Courier New\",monospace;
        }
        h1.error, h2.error, h3.error, h4.error, h5.error {
            color: red;
        }
        .panel-default>.panel-heading {
            color: #FFF;
            background-color: #428bca;
            border-color: #428bca;
        }
        .row {
            margin-right: 0px;
            margin-left: 0px;
        }
        .header {
            background: #fff url(\"https://www.paypalobjects.com/webstatic/developer/banners/Braintree_desktop_BG_2X.jpg\") no-repeat top right;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            color: #EEE;
        }
        .header a:link, .header a:visited, .header a:hover, .header a:active {
            text-decoration: none;

        }
        /* Sticky footer styles
-------------------------------------------------- */
        html {
            position: relative;
            min-height: 100%;
        }

        body {
            /* Margin bottom by footer height */
            margin-bottom: 60px;
        }

        .footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            /* Set the fixed height of the footer here */
            min-height: 60px;
            padding-top: 15px;
        }
        .footer .footer-links, .footer .footer-links li {
            display: inline-block;
            padding: 5px 8px;
            font-size: 110%;
        }
        .footer a {
            text-decoration: none;
        }

        .string { color: #428bca; }
        .number { color: darkorange; }
        .boolean { color: blue; }
        .null { color: magenta; }
        .key { color: slategray; font-weight: bold; }
        .id { color: red; }

    </style>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
    <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
    <![endif]-->

    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>
    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js\"></script>
    <script src=\"//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.2.0/js/scrollspy.min.js\"></script>
    <script>
        $( document ).ready(function() {
            $(\"#accordion .panel-collapse:last\").collapse('toggle');

            $(document.body).append('<footer class=\"footer\"> <div class=\"container\"> <div class=\"footer-div\"> <ul class=\"footer-links\"> <li> <a href=\"http://paypal.github.io/PayPal-PHP-SDK/\" target=\"_blank\"><i class=\"fa fa-github\"></i> PayPal PHP SDK</a></li><li> <a href=\"https://developer.paypal.com/webapps/developer/docs/api/\" target=\"_blank\"><i class=\"fa fa-book\"></i> REST API Reference</a> </li><li> <a href=\"https://github.com/paypal/PayPal-PHP-SDK/issues\" target=\"_blank\"><i class=\"fa fa-exclamation-triangle\"></i> Report Issues </a> </li></ul> </div></div></footer>');


            $(\".prettyprint\").each(function() {
                $(this).html(syntaxHighlight($(this).html()));
            });

        });


        /* http://stackoverflow.com/questions/4810841/how-can-i-pretty-print-json-using-javascript */
        function syntaxHighlight(json) {
            json = json.replace(/&/g, '&').replace(/</g, '&lt;').replace(/>/g, '&gt;');
            return json.replace(/(\"(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\\"])*\"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
                var cls = 'number';
                if (/^\"/.test(match)) {
                    if (/:$/.test(match)) {
                        cls = 'key';
                        if (match == '\"id\":') {
                            console.log(\"Matched ID\" + match);
                            cls = 'key id';
                        }
                    } else {
                        cls = 'string';
                    }
                } else if (/true|false/.test(match)) {
                    cls = 'boolean';
                } else if (/null/.test(match)) {
                    cls = 'null';
                }
                return '<span class=\"' + cls + '\">' + match + '</span>';
            });
        }
    </script>
</head>
<body>";
                echo '
                  <div class="row header"><div class="col-md-5 pull-left"><br /><a href="../index.php"><h1 class="home">&#10094;&#10094; Back to Samples</h1></a><br /></div> <br />
                  <div class="col-md-4 pull-right"><img src="https://www.paypalobjects.com/webstatic/developer/logo2_paypal_developer_2x.png" class="logo" width="300"/></div> </div>';
                echo '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">';
            }
            self::$printResultCounter++;
            echo '
        <div class="panel panel-default">
            <div class="panel-heading '. ($errorMessage ? 'error' : '') .'" role="tab" id="heading-'.self::$printResultCounter.'">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#step-'. self::$printResultCounter .'" aria-expanded="false" aria-controls="step-'.self::$printResultCounter.'">
            '. self::$printResultCounter .'. '. $title . ($errorMessage ? ' (Failed)' : '') . '</a>
                </h4>
            </div>
            <div id="step-'.self::$printResultCounter.'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-'. self::$printResultCounter . '">
                <div class="panel-body">
            ';

            if ($objectId) {
                echo "<div>" . ($objectName ? $objectName : "Object") . " with ID: $objectId </div>";
            }

            echo '<div class="row hidden-xs hidden-sm hidden-md"><div class="col-md-6"><h4>Request Object</h4>';
            self::printObject($request);
            echo '</div><div class="col-md-6"><h4 class="'. ($errorMessage ? 'error' : '') .'">Response Object</h4>';
            self::printObject($response, $errorMessage);
            echo '</div></div>';

            echo '<div class="hidden-lg"><ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" ><a href="#step-'.self::$printResultCounter .'-request" role="tab" data-toggle="tab">Request</a></li>
                        <li role="presentation" class="active"><a href="#step-'.self::$printResultCounter .'-response" role="tab" data-toggle="tab">Response</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane" id="step-'.self::$printResultCounter .'-request"><h4>Request Object</h4>';
            self::printObject($request) ;
            echo '</div><div role="tabpanel" class="tab-pane active" id="step-'.self::$printResultCounter .'-response"><h4>Response Object</h4>';
            self::printObject($response, $errorMessage);
            echo '</div></div></div></div>
            </div>
        </div>';
        }
        flush();
    }

    /**
     * Prints success response HTML Output to web page.
     *
     * @param string     $title
     * @param string    $objectName
     * @param string    $objectId
     * @param mixed     $request
     * @param mixed     $response
     */
    public static function printResult($title, $objectName, $objectId = null, $request = null, $response = null)
    {
        self::printOutput($title, $objectName, $objectId, $request, $response, false);
    }

    /**
     * Prints Error
     *
     * @param      $title
     * @param      $objectName
     * @param null $objectId
     * @param null $request
     * @param \Exception $exception
     */
    public static function printError($title, $objectName, $objectId = null, $request = null, $exception = null)
    {
        $data = null;
        if ($exception instanceof \PayPal\Exception\PayPalConnectionException) {
            $data = $exception->getData();
        }
        self::printOutput($title, $objectName, $objectId, $request, $data, $exception->getMessage());
    }

    protected static function printConsoleObject($object, $error = null)
    {
        if ($error) {
            echo 'ERROR:'. $error;
        }
        if ($object) {
            if (is_a($object, 'PayPal\Common\PayPalModel')) {
                /** @var $object \PayPal\Common\PayPalModel */
                echo $object->toJSON(128);
            } elseif (is_string($object) && \PayPal\Validation\JsonValidator::validate($object, true)) {
                echo str_replace('\\/', '/', json_encode(json_decode($object), 128));
            } elseif (is_string($object)) {
                echo $object;
            } else {
                print_r($object);
            }
        } else {
            echo "No Data";
        }
    }

    protected static function printObject($object, $error = null)
    {
        if ($error) {
            echo '<p class="error"><i class="fa fa-exclamation-triangle"></i> '.
                $error.
                '</p>';
        }
        if ($object) {
            if (is_a($object, 'PayPal\Common\PayPalModel')) {
                /** @var $object \PayPal\Common\PayPalModel */
                echo '<pre class="prettyprint '. ($error ? 'error' : '') .'">' . $object->toJSON(128) . "</pre>";
            } elseif (is_string($object) && \PayPal\Validation\JsonValidator::validate($object, true)) {
                echo '<pre class="prettyprint '. ($error ? 'error' : '') .'">'. str_replace('\\/', '/', json_encode(json_decode($object), 128)) . "</pre>";
            } elseif (is_string($object)) {
                echo '<pre class="prettyprint '. ($error ? 'error' : '') .'">' . $object . '</pre>';
            } else {
                echo "<pre>";
                print_r($object);
                echo "</pre>";
            }
        } else {
            echo "<span>No Data</span>";
        }
    }
}

/**
 * ### getBaseUrl function
 * // utility function that returns base url for
 * // determining return/cancel urls
 *
 * @return string
 */
function getBaseUrl()
{
    if (PHP_SAPI == 'cli') {
        $trace=debug_backtrace();
        $relativePath = substr(dirname($trace[0]['file']), strlen(dirname(dirname(__FILE__))));
        echo "Warning: This sample may require a server to handle return URL. Cannot execute in command line. Defaulting URL to http://localhost$relativePath \n";
        return "http://localhost" . $relativePath;
    }
    $protocol = 'http';
    if ($_SERVER['SERVER_PORT'] == 443 || (!empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on')) {
        $protocol .= 's';
    }
    $host = $_SERVER['HTTP_HOST'];
    $request = $_SERVER['PHP_SELF'];
    return dirname($protocol . '://' . $host . $request);
}
