<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                '<controller>/<action>' => '<controller>/<action>'
            ],
        ],

        'payPalRest' => [
            'class' => 'kun391\paypal\RestAPI',
            'pathFileConfig' => '../../common/config/paypalConfig.php',
            'successUrl' => 'http://localhost/yii2_advanced_test/frontend/web/', //full url action return url
            'cancelUrl' => 'http://localhost/yii2_advanced_test/frontend/web/' //full url action return url
        ],

    ],
];
