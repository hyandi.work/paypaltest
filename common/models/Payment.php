<?php

namespace common\models;

use frontend\models\paypal\ResultPrinter;
use PayPal\Api\PaymentExecution;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use PayPal\Api\Amount;
use PayPal\Api\Payer;
use PayPal\Api\Payment as PayPalPayment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;

/**
 * Class Payment
 * @package common\models
 *
 * @property integer id
 * @property integer user_id
 * @property string request_id
 * @property integer status
 * @property integer created_at
 * @property integer updated_at
 */
class Payment extends ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_PAY = 10;

    private $_api;

    public function __construct(array $config = [])
    {
        $this->_api = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                'AR9iEUREP23sO9zLgRx36rP03v-PQw4-Co-V3dreoSF9WrcgxIc1FYU6BsAWCfF6yLsi745m_uMdPIeL',     // ClientID
                'EJDEcezoom99Y6lLnt1CDqz913MFjOShGN7pH8IoCfL1BB7EBSYfWB0jQwMqwilkEVjRAjBhtcsiQtOV'      // ClientSecret
            )
        );
        $this->_api->setConfig(array('service.EndPoint'=>"https://api.sandbox.paypal.com", 'cache.enabled'=>false));
        parent::__construct($config);
    }

    public static function tableName()
    {
        return '{{%payment}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_NEW],
            ['status', 'in', 'range' => [self::STATUS_NEW, self::STATUS_PAY]]
        ];
    }

    public function beforeSave($insert)
    {
        $this->user_id = Yii::$app->user->id;
        return parent::beforeSave($insert);
    }

    public static function find()
    {
        return parent::find()->where(['user_id' => Yii::$app->getUser()->id]);
    }

    public function getUser()
    {
        $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Get payment by request id
     * @param $id
     * @return static
     */
    public static function findByRequestID($id)
    {
        return static::findOne(['request_id' => $id, 'status' => static::STATUS_NEW]);
    }

    /**
     * @return bool
     */
    public static function isPay()
    {
        $res = static::find()->where(['status' => static::STATUS_PAY, 'user_id' => Yii::$app->getUser()->id])->count();
        return $res != 0;
    }

    /**
     * Accept payment
     * @param $payment_id
     * @param $token
     * @param $payer_id
     */
    public function execute($payment_id, $token, $payer_id)
    {
        $payment = PayPalPayment::get($payment_id, $this->_api);
        $execution = new PaymentExecution();
        $execution->setPayerId($payer_id);
        try {
            $result = $payment->execute($execution, $this->_api);
            if($result->state == 'approved'){
                $this->status = static::STATUS_PAY;
                $this->save();
            }
            //ResultPrinter::printResult("Executed Payment", "Payment", $payment->getId(), $execution, $result);
        }catch (\Exception $ex){
            ResultPrinter::printError("Executed Payment", "Payment", null, null, $ex);
        }
    }

    /**
     * create payment
     * @return null|string
     */
    public function pay()
    {
        try {
            $payer = new Payer();
            $payer->setPaymentMethod("paypal");
            $amount = new Amount();
            $amount->setCurrency("USD")
                ->setTotal(20);
            $transaction = new Transaction();
            $transaction->setAmount($amount);
            $baseUrl = $this->getBaseUrl();
            $redirectUrls = new RedirectUrls();
            $redirectUrls->setReturnUrl("$baseUrl/paypal/success?success=true")
                ->setCancelUrl("$baseUrl/paypal/fail?success=false");
            $payment = new PayPalPayment();
            $payment->setIntent("sale")
                ->setPayer($payer)
                ->setRedirectUrls($redirectUrls)
                ->setTransactions(array($transaction));

            $request = clone $payment;
            $curl_info = curl_version();

            $payment->create($this->_api);
            $this->request_id = $payment->id;
            $this->save();
        } catch (\Exception $ex) {
            ResultPrinter::printError("FAILURE: SECURITY WARNING: TLSv1.2 is not supported on this system. Please upgrade your curl to atleast 7.34.0.<br /> - Current Curl Version: " . $curl_info['version'] . "<br /> - Current OpenSSL Version:" . $curl_info['ssl_version'], "Payment", null, $request, $ex);
            exit(1);
        }
        return $payment->getApprovalLink();
    }

    /**
     * get URL
     * @return string
     */
    private function getBaseUrl()
    {
        if (PHP_SAPI == 'cli') {
            $trace=debug_backtrace();
            $relativePath = substr(dirname($trace[0]['file']), strlen(dirname(dirname(__FILE__))));
            echo "Warning: This sample may require a server to handle return URL. Cannot execute in command line. Defaulting URL to http://localhost$relativePath \n";
            return "http://localhost" . $relativePath;
        }
        $protocol = 'http';
        if ($_SERVER['SERVER_PORT'] == 443 || (!empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on')) {
            $protocol .= 's';
        }
        $host = $_SERVER['HTTP_HOST'];
        $request = $_SERVER['PHP_SELF'];

        return dirname($protocol . '://' . $host . $request);
    }
}